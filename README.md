# Password generator

## Purpose

The purpose of this tool is to derive a password from a secret, so that it is not necessary to remember it or store it in a vault.

You would typically generate a password for each domain where you own an account.

## Example

```console
$ python -m genpwd -d github.com -d gitlab.com
Type your secret:
Type your secret again:

Using 3 iterations

github.com                       S#Ah@Wj2fFriTDl0j3C^$9nfww11@^hm
gitlab.com                       Lmt7hsP0ejUwpFYROGG5jyX^jynVHV8A
```

## How does it work?

1. The secret is first salted with the provided domain name to give the following input string: `_yoursecret_@domain'.
2. A hash of length L is computed for all rotations of the input string, yielding N hashes, where N is equal to the length of the input string.
3. The ith digits of each of the N hashes are concatenated to form an N-digit hexadecimal number and its value is used to derive the ith character of the password.
4. A password of length L is then obtained, which can be used as input to another iteration of the process, starting again at step 2.
5. The process is repeated a configurable number of times to yield the final password.

## Is it really safe?

I don't know. Use this at your own risk.
