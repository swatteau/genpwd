import hashlib

# The set of characters allowed in the generated password
CHARSET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY0123456789&#^@$%*"
# The length of the hashes, which ultimately corresponds to the length of the password
HASH_LEN = 32
# The number of iterations used to obtain the final password
N_ITER = 3

class Generator:
    def __init__(self, charset, hash_len, n_iter):
        """Constructor"""
        self.charset = charset
        self.hash_len = hash_len
        self.n_iter = n_iter

    def hash(self, text):
        """Hashes the given text"""
        return hashlib.sha3_512(text.encode()).hexdigest()[:self.hash_len]

    def rotate(self, text):
        """Returns the list of rotations of the given text"""
        for i in range(0, len(text)):
            yield text[i:] + text[0:i]

    def transform(self, text):
        """Applies the transform to the given text"""
        pwd = ''
        hashes = [self.hash(r) for r in self.rotate(text)]
        for i in range(self.hash_len):
            k = int(''.join([h[i] for h in hashes]), 16)
            pwd += CHARSET[k % len(CHARSET)]
        return pwd

    def make_password(self, text):
        """Generates a password by applying the transform a given number of times"""
        pwd = text
        for _ in range(self.n_iter):
            pwd = self.transform(pwd)
        return pwd

if __name__ == '__main__':
    import argparse
    import getpass
    import sys

    # Parse the command line options
    parser = argparse.ArgumentParser(description='Password generator')
    parser.add_argument('-d', '--domain', required=True, action='append', help='domain name')
    args = parser.parse_args()

    # Get the secret from the user
    secret = getpass.getpass('Type your secret: ')
    confirm = getpass.getpass('Type your secret again: ')
    if secret != confirm:
        print("ERROR: you did not type the same secret twice.")
        sys.exit(-1)

    # Number of iterations
    print()
    print(f'Using {N_ITER} iterations')
    print()

    # Generate the password
    gen = Generator(CHARSET, HASH_LEN, N_ITER)
    for domain in args.domain:
        salted_secret = secret + '@' + domain
        pwd = gen.make_password(salted_secret)
        print(f'{domain:32} {pwd:32}')

    sys.exit(0)
